#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#-------------------------------------------------------------------------------
# Name:        funscapy
# Purpose:     Pleins de petites fonctions pour apprendre scapy
#
# Author:      elfab
#
#
#-------------------------------------------------------------------------------
import sys
import os
from scapy.all import *

# Fonction d'affichage du menu principal
def menu_p():
    print("Bienvenue ! \nMenu principal")
    print("1 : Ping d'une adresse IP")
    print("2 : Scan d'une plage d'IP avec ping")
    print("0 : Quitter")
    choix=input("Votre choix ? :")
    if choix==0:
        sys.exit()
    if choix==1:
        ping_ip()
    if choix==2:
        scan_plage()

# Fonction de ping d'une adresse IP
def ping_ip():
    IPDestination=raw_input('Entrez une IP de destination pour le ping :') #On demande l'ip � laquelle envoyer le ping
    mon_ping = Ether() / IP(dst=IPDestination) / ICMP() # on cr�e le format de la trame a envoyer
    mon_ping.show() # on l'affiche (pour comprendre !)
    sendp(mon_ping) # on l'envoie
    rep= srp1(mon_ping,timeout=0.5) #Dans la r�ponse, on r�cup�re la trame retour
    if isinstance(rep, types.NoneType): # On teste les attributs de l'objet rep, s'ils sont vides, c'est que nous n'avons pas de r�ponse au ping
        print("Pas de reponse")
    else:
        print rep.src,' a repondu'
    menu_p()
    

# Fonction de scan de plage d'adresses IP par ping
def scan_plage():
    IPrange=raw_input("Entrez la plage d'IP a scanner (ex.: 192.168.1.1-15) :'") #On demande l'ip � laquelle envoyer le ping
    rep,non_rep = sr( IP(dst=IPrange) / ICMP() , timeout=0.5 )
    for elem in rep : # elem repr�sente un couple (paquet �mis, paquet re�u)
        if elem[1].type == 0 : # 0 <=> echo-reply
            print elem[1].src + ' a renvoye un echo-reply '
    menu_p()
    

# Lancement du programme
menu_p()