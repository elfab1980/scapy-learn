#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#-------------------------------------------------------------------------------
# Name:        funscapy
# Purpose:     Pleins de petites fonctions pour apprendre scapy
#
# Author:      elfab
#
# Thx : siteduzero.com, et surtout FireZero, auteur du super tuto sur scapy...
#-------------------------------------------------------------------------------
import sys
import os
from scapy.all import *

# Fonction d'affichage du menu principal
def menu_p():
    print("\nBienvenue ! \nMenu principal")
    print("1 : Ping d'une adresse IP")
    print("2 : Scan d'une plage d'IP avec ping")
    print("3 : Scan d'une plage de ports par paquets SYN")
    print("4 : Traceroute")
    print("0 : Quitter")
    choix=input("Votre choix ? :")
    if choix==0:
        sys.exit()
    if choix==1:
        ping_ip()
    if choix==2:
        scan_plage()
    if choix==3:
        syn_ports()
    if choix==4:
        traceroute()

# Fonction de ping d'une adresse IP
def ping_ip():
    IPDestination=raw_input('Entrez une IP de destination pour le ping :') #On demande l'ip � laquelle envoyer le ping
    mon_ping = Ether() / IP(dst=IPDestination) / ICMP() # on cr�e le format de la trame a envoyer
    mon_ping.show() # on l'affiche (pour comprendre !)
    sendp(mon_ping) # on l'envoie
    rep= srp1(mon_ping,timeout=0.5) #Dans la r�ponse, on r�cup�re la trame retour
    if isinstance(rep, types.NoneType): # On teste les attributs de l'objet rep, s'ils sont vides, c'est que nous n'avons pas de r�ponse au ping
        print("Pas de reponse")
    else:
        print rep.src,' a repondu'
    menu_p()
    

# Fonction de scan de plage d'adresses IP par ping
def scan_plage():
    IPrange=raw_input("Entrez la plage d'IP a scanner (ex.: 192.168.1.1-15) :'") #On demande l'ip � laquelle envoyer le ping
    rep,non_rep = sr( IP(dst=IPrange) / ICMP() , timeout=0.5 )
    for elem in rep : # elem repr�sente un couple (paquet �mis, paquet re�u)
        if elem[1].type == 0 : # 0 <=> echo-reply
            print elem[1].src + ' a renvoye un echo-reply '
    menu_p()

# Fonction de scan des ports par envoi de paquets SYN
def syn_ports():
    ip=raw_input("Entrez l'IP pour le scan de ports : ")
    debut=int(input("Port de debut ? : "))
    fin=int(input("Port de fin ? : "))
    rep,non_rep=sr(IP(dst=ip)/TCP(dport=(debut,fin),flags='S'))
    for emis,recu in rep :
        if recu[1].flags==18 : # 18 <=> SYN+ACK
            print 'port ouvert : ', recu[1].sport
    menu_p()


def traceroute():
    ip=raw_input("Entrez une IP de destination : ")
    rep,non_rep=sr( IP(dst=ip, ttl=(1,25)) / TCP(), timeout=1 )
    for emis,recu in rep :
        print emis.ttl, recu.src
    menu_p()
# Lancement du programme
menu_p()
