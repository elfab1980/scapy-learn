#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#-------------------------------------------------------------------------------
# Name:        scapysniff
# Purpose:     Pleins de petites fonctions pour apprendre scapy
#
# Author:      elfab
#
# Thx : siteduzero.com, et surtout FireZero, auteur du super tuto sur scapy...
#-------------------------------------------------------------------------------
import sys
import os
from scapy.all import *

# Fonction d'affichage du menu principal
def menu_p():
    print("\nBienvenue ! \nMenu principal")
    print("1 : Sniff d'une IP")
    print("0 : Quitter")
    choix=input("Votre choix ? :")
    if choix==0:
        sys.exit()
    if choix==1:
        sniff_ip()

def sniff_ip():
    ip=raw_input("IP a renifler ? (ctrl+c pour arreter et afficher le resultat) : ")
    host="host "+ip
    rep=sniff(filter=host)
    rep.show()
    rep[0].show()
# Lancement du programme
menu_p()
